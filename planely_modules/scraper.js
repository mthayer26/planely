var async = require('async');
var cheerio = require('cheerio');
var fs = require('fs');
var _ = require('underscore');
var mongodb = require('mongodb');
var request = require('request');
var request = request.defaults({
    jar: true
});
var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var secrets = require('../config/secrets');

var parse = require('./parser');
var uri = 'mongodb://mthayer:planesPadres26@ds033069.mongolab.com:33069/planes';

// 1) Get the number of list pages for a makes
//      pageCountScrape()
// returns number, make
// 2) Get links to detail pages from list page
//      get the number of pages from pageCountScrape() and scrapes details urls returns an array
// 3) Get html from detail page

exports.planeScrape = function(req, res, next) {
    pageCountScrape('Diamond');
    res.redirect('/');
}

//gets the number of list pages for airplane make

exports.pageCountScrape = function(make) {
    pageUrls = [];
    request.get('http://www.trade-a-plane.com/search?s-advanced=yes&s-custom=&sale_status=For+Sale&make='+make+'&s-type=aircraft&s-page_size=25', function(err, request, body) {
        if (err) return next(err);
        $ = cheerio.load(body);
        $('.paging li').each(function() {
            pageUrls.push($(this).find('a').attr('href'));
        });
        lastUrl = pageUrls.slice(-1).pop();
        numPages = lastUrl.split('page=');
        pages = {
            'make': make,
            'numPages': numPages[1]
        }
        console.log(pages);
        pageListScrape(pages);
    });
}

//get all the plane list pages and stores html

var pageListScrape = function(pages) {
    var url_seg = 'http://www.trade-a-plane.com/search?s-advanced=yes&s-custom=&sale_status=For+Sale&make='+pages.make+'&s-type=aircraft&s-page_size=25&s-page='
    console.log("Scraping list pages...")
    details = [];
    var complete_requests = 0;
    for (i = 1; i <= pages.numPages; i++) {
        if(i == 1){url_seg = 'http://www.trade-a-plane.com/search?s-advanced=yes&s-custom=&sale_status=For+Sale&make='+pages.make+'&s-type=aircraft&s-page_size=25'}
            else {url_seg = 'http://www.trade-a-plane.com/search?s-advanced=yes&s-custom=&sale_status=For+Sale&make='+pages.make+'&s-type=aircraft&s-page_size=25&s-page='};
        console.log("getting page "+i);
        request(url_seg + i, function(error, response, body) {
            if (!error && response.statusCode == 200) {
                $ = cheerio.load(body);
                $('.result').each(function() {

                    details.push($(this).find('.main a').attr('href'));
                   
                })
                complete_requests++;
                if (complete_requests == pages.numPages){
                    console.log("Found detail page links: ");
                    console.log(details);
                    //need to change the all caps make to first letter caps
                    var make_readable = pages.make.toLowerCase();            
                    var make_readable = make_readable.charAt(0).toUpperCase() + make_readable.slice(1); 

                    var scrape = new Scrape({
                        make_upper: pages.make,
                        make: make_readable,
                        total_planes: details.length,
                        urls_to_parse: details
                    })
                    scrape.save(function(err, scrape_entry){
                        if(err){return};
                        console.log("scrape details saved")
                        all_details = {
                            details_urls: details,
                            make: pages.make,
                            scrape_id: scrape_entry.id,
                            status: 'Page getter pending'
                        }
                        page_getter(all_details);
                    })
                }
            }
        })
    }
}

var page_getter = function(all_details){
    var complete_requests = 0;
    console.log("Getting all plane details for "+all_details.details_urls.length+" planes");
    for (i = 0; i < all_details.details_urls.length; ++i) {
        setTimeout(function(i){
            request("http://www.trade-a-plane.com"+details[i], function(error, response, body) {
                if (!error && response.statusCode == 200) {
                    var page = new Page({
                        body_html: body,
                        make: all_details.make,
                        url: response.request.href,
                        scrape_id: all_details.scrape_id
                    });
                    page.save(function(err) {
                        if (err) return next(err);
                        console.log("page saved");
                        complete_requests++;
                        console.log(complete_requests);
                        console.log(all_details.details_urls.length);
                        if (complete_requests == all_details.details_urls.length){
                            console.log("DONE");
                            parse_pages(all_details);
                        }
                    });      
                }else{console.log(error);}
            })
        }, 1000*i, i)
    } 
}

var parse_pages = function(all_details){
// exports.parse_pages = function(all_details){
    Page.find({'scrape_id': all_details.scrape_id }, 'body_html', function(err, pages){
    // Page.find({'scrape_id': '56d3c6e89f1d080d32756c00' }, 'body_html', function(err, pages){
        var counter = 1;
        console.log('Starting to parse '+pages.length+ 'pages');
        pages.forEach( function (page) {
            plane = [];
            console.log('Parcing plane data');
            $ = cheerio.load(page.body_html);
    
            plane.description = $('#detailed_desc').find('pre[itemprop = "description"]').text();
            plane.avionics = $('#avionics_equipment').find('pre').text();
            plane.interior = $('#interior_exterior').find('pre').text();
            plane.airframe_tt = Number($("label:contains('Total Time:')").parent().clone().children().remove().end().text().replace(/[^0-9\.]+/g,""));
            plane.engine_tt = Number($("label:contains('Engine 1 Overhaul Time:')").parent().clone().children().remove().end().text().replace(/[^0-9\.]+/g,""));
            plane.prop_tt = Number($("label:contains('Prop 1 Overhaul Time:')").parent().clone().children().remove().end().text().replace(/[^0-9\.]+/g,""));
            plane.flight_rules = $("label:contains('Flight Rules:')").parent().clone().children().remove().end().text();
            plane.year = Number($("label:contains('Year:')").parent().clone().children().remove().end().text().replace(/[^0-9\.]+/g,""));
            plane.reg_number = $("label:contains('Registration #:')").parent().clone().children().remove().end().text().trim();
            plane.model = $(".makeModel").clone().children().remove().end().text().trim();
            plane.model_lower = plane.model.toLowerCase()
            plane.make = $(".makeModel").find('span[itemprop = "name"]').text()
            plane.make_lower = plane.make.toLowerCase();            
            plane.make = plane.make_lower.charAt(0).toUpperCase() + plane.make_lower.slice(1);

            location = $("label:contains('Location:')").parent().clone().children().remove().end().text();
            location = location.replace(/(\r\n|\n|\r)/gm,"");
            plane.location = location.replace(/ +(?= )/g,'');

            var price = $('.price').find('span[itemprop = "price"]').text();
                    price = price.trim();
                    if (price == 'Call for Price' || price == '') {plane.price = null}
                        else {
                            plane.price = parseInt(price.replace(/[^0-9\.]+/g, ""));
                        };

            var plane = new Plane({
                plane_all: plane,
                make: plane.make,
                make_readable: plane.make_readable,
                make_lower: plane.make_lower,
                avionics: plane.avionics,
                engines: plane.engine,
                year: plane.year,
                model: plane.model,
                model_lower: plane.model_lower,
                manufacturer: plane.make,
                interior: plane.interior,
                exterior: plane.exterior,
                price: plane.price,
                reg_number: plane.reg_number,
                airframe_tt: plane.airframe_tt,
                engine_tt: plane.engine_tt,
                prop_tt: plane.prop_tt,
                location: plane.location,
                flight_rules: plane.flight_rules,
                description: plane.description,
                scrape_id: all_details.scrape_id 
            });
            
            plane.save(function(err) {
                if (err) {
                    console.log(err);
                    return next(err)
                };
                console.log("plane saved "+counter);
                counter++;
                console.log("counter");
                if(counter == pages.length){
                    Scrape.findOneAndUpdate({'_id': all_details.scrape_id}, {'status': 'Parse pending'}, {upsert:false}, function(err, doc){
                        if (err) return res.send(500, { error: err });
                        parse.parsePricesAveForModel(all_details.make, all_details.scrape_id);
                        // return res.send("succesfully updated");
                        console.log("DONE PARSING AND SAVING PLANES")
                    });
                }
            });
        })
    })
}

