var secrets = require('../config/secrets');
var stripe = require('stripe')(secrets.stripe.secretKey);
var nodemailer = require('nodemailer');
var emails = require('../config/emails');
var User = require('../models/User');
var Report = require('../models/Report');
var PurchasedReport = require('../models/PurchasedReport');
var GeneratedReport = require('../models/GeneratedReport');
var _ = require('lodash');

/**
 * GET /
 * Home page.
 */
exports.index = function(req, res) {
  if (req.user) {
    res.redirect('/myreports');
  }else {
    Report.find({'published': true}, function(err, reports){
      res.render('home', {
        title: 'Home',
        publishableKey: secrets.stripe.publishableKey,
        reports: reports
      });
      
    })
  }
};

exports.myreports = function(req, res) {

  User.findById(req.user.id).select('purchased_reports').exec(function(err, user) {
    purchased_reports = _.map(user.purchased_reports, 'report_id');
    GeneratedReport.find({ _id: { $in: purchased_reports}}, function(err, purchased_reports){
      GeneratedReport.find({'details.published' : true, 'details.free_promo' : true}).sort({'date': -1}).limit(1).exec(function(err, availableReports){
        console.log()
        if (purchased_reports.length < 1) {purchased_reports = null}; 
        res.render('myreports', {
          title: 'My Reports',
          reports: purchased_reports,
          publishableKey: secrets.stripe.publishableKey,
          availableReports: availableReports
        });
        
      })
    }) 
  })
}


exports.myreportsById = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('cover', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsGeneral = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('general', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsPrice = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('price', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsCosts = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('costs', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsMarket = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('market', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsAds = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('ads', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.myreportsLocations = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.findById(req.params.id, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    res.render('locations', {
      title: 'My Reports',
      report: report
    });
      
  })
}

exports.postStripe = function(req, res, next) {

    var stripeToken = req.body.stripeToken;
    var stripeEmail = req.body.stripeEmail;
    console.log(req.body);
    stripe.charges.create({
        amount: 3900,
        currency: 'usd',
        card: stripeToken,
        receipt_email: stripeEmail,
        description: 'Buyers Report',
        metadata: {
            'report_id': req.body.report_id
        } ///this is where we pass the report_id not on the form javascript
    }, function(err, charge) {
        if (err && err.type === 'StripeCardError') {
            req.flash('errors', {
                msg: 'Your card has been declined.'
            });
            res.redirect('/myreports');
        }
        //generate report for user by adding report id to purchased reports in user document
        console.log(charge);

        User.findByIdAndUpdate(
            req.user.id, {
                $push: {
                    "purchased_reports": {
                        report_id: charge.metadata.report_id,
                        date: Date.now()
                    }
                }
            }, {
                safe: true,
                upsert: true
            },
            function(err, user) {
                console.log(err);
                var pr = new PurchasedReport({
                    report_id: charge.metadata.report_id,
                    user_id: req.user.id,
                    charge: charge
                });

                pr.save(function(err) {
                    if (err) {
                        console.log(err)
                    }
                    req.flash('success', {
                        msg: 'Your card was successfully charged. Thank you for purchasing.'
                    });
                    res.redirect('/myreports');

                })

                var transporter = nodemailer.createTransport({
                    service: 'SendGrid',
                    auth: {
                        user: secrets.sendgrid.user,
                        pass: secrets.sendgrid.password
                    }
                });

                var mailOptions = {
                    to: req.user.email,
                    from: '"Planeio ✈" <info@planeio.com>',
                    subject: 'Thank you for purchasing your buyers guide!',
                    html: '<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD XHTML 1.0 Strict\/\/EN\" \"http:\/\/www.w3.org\/TR\/xhtml1\/DTD\/xhtml1-strict.dtd\"><html xmlns=\"http:\/\/www.w3.org\/1999\/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text\/html; charset=utf-8\"><meta name=\"viewport\" content=\"width=device-width\"><\/head><body style=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important\"><style>@media only screen and (max-width:596px){.small-float-center{margin:0 auto!important;float:none!important;text-align:center!important}.small-text-center{text-align:center!important}.small-text-left{text-align:left!important}.small-text-right{text-align:right!important}}@media only screen and (max-width:596px){table.body table.container .hide-for-large{display:block!important;width:auto!important;overflow:visible!important}}@media only screen and (max-width:596px){table.body table.container .row.hide-for-large{display:table!important;width:100%!important}}@media only screen and (max-width:596px){table.body table.container .show-for-large{display:none!important;width:0;mso-hide:all;overflow:hidden}}@media only screen and (max-width:596px){table.body img{width:auto!important;height:auto!important}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .column,table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:16px!important;padding-right:16px!important}table.body .column .column,table.body .column .columns,table.body .columns .column,table.body .columns .columns{padding-left:0!important;padding-right:0!important}table.body .collapse .column,table.body .collapse .columns{padding-left:0!important;padding-right:0!important}td.small-1,th.small-1{display:inline-block!important;width:8.33333%!important}td.small-2,th.small-2{display:inline-block!important;width:16.66667%!important}td.small-3,th.small-3{display:inline-block!important;width:25%!important}td.small-4,th.small-4{display:inline-block!important;width:33.33333%!important}td.small-5,th.small-5{display:inline-block!important;width:41.66667%!important}td.small-6,th.small-6{display:inline-block!important;width:50%!important}td.small-7,th.small-7{display:inline-block!important;width:58.33333%!important}td.small-8,th.small-8{display:inline-block!important;width:66.66667%!important}td.small-9,th.small-9{display:inline-block!important;width:75%!important}td.small-10,th.small-10{display:inline-block!important;width:83.33333%!important}td.small-11,th.small-11{display:inline-block!important;width:91.66667%!important}td.small-12,th.small-12{display:inline-block!important;width:100%!important}.column td.small-12,.column th.small-12,.columns td.small-12,.columns th.small-12{display:block!important;width:100%!important}.body .column td.small-1,.body .column th.small-1,.body .columns td.small-1,.body .columns th.small-1,td.small-1 center,th.small-1 center{display:inline-block!important;width:8.33333%!important}.body .column td.small-2,.body .column th.small-2,.body .columns td.small-2,.body .columns th.small-2,td.small-2 center,th.small-2 center{display:inline-block!important;width:16.66667%!important}.body .column td.small-3,.body .column th.small-3,.body .columns td.small-3,.body .columns th.small-3,td.small-3 center,th.small-3 center{display:inline-block!important;width:25%!important}.body .column td.small-4,.body .column th.small-4,.body .columns td.small-4,.body .columns th.small-4,td.small-4 center,th.small-4 center{display:inline-block!important;width:33.33333%!important}.body .column td.small-5,.body .column th.small-5,.body .columns td.small-5,.body .columns th.small-5,td.small-5 center,th.small-5 center{display:inline-block!important;width:41.66667%!important}.body .column td.small-6,.body .column th.small-6,.body .columns td.small-6,.body .columns th.small-6,td.small-6 center,th.small-6 center{display:inline-block!important;width:50%!important}.body .column td.small-7,.body .column th.small-7,.body .columns td.small-7,.body .columns th.small-7,td.small-7 center,th.small-7 center{display:inline-block!important;width:58.33333%!important}.body .column td.small-8,.body .column th.small-8,.body .columns td.small-8,.body .columns th.small-8,td.small-8 center,th.small-8 center{display:inline-block!important;width:66.66667%!important}.body .column td.small-9,.body .column th.small-9,.body .columns td.small-9,.body .columns th.small-9,td.small-9 center,th.small-9 center{display:inline-block!important;width:75%!important}.body .column td.small-10,.body .column th.small-10,.body .columns td.small-10,.body .columns th.small-10,td.small-10 center,th.small-10 center{display:inline-block!important;width:83.33333%!important}.body .column td.small-11,.body .column th.small-11,.body .columns td.small-11,.body .columns th.small-11,td.small-11 center,th.small-11 center{display:inline-block!important;width:91.66667%!important}table.body td.small-offset-1,table.body th.small-offset-1{margin-left:8.33333%!important;Margin-left:8.33333%!important}table.body td.small-offset-2,table.body th.small-offset-2{margin-left:16.66667%!important;Margin-left:16.66667%!important}table.body td.small-offset-3,table.body th.small-offset-3{margin-left:25%!important;Margin-left:25%!important}table.body td.small-offset-4,table.body th.small-offset-4{margin-left:33.33333%!important;Margin-left:33.33333%!important}table.body td.small-offset-5,table.body th.small-offset-5{margin-left:41.66667%!important;Margin-left:41.66667%!important}table.body td.small-offset-6,table.body th.small-offset-6{margin-left:50%!important;Margin-left:50%!important}table.body td.small-offset-7,table.body th.small-offset-7{margin-left:58.33333%!important;Margin-left:58.33333%!important}table.body td.small-offset-8,table.body th.small-offset-8{margin-left:66.66667%!important;Margin-left:66.66667%!important}table.body td.small-offset-9,table.body th.small-offset-9{margin-left:75%!important;Margin-left:75%!important}table.body td.small-offset-10,table.body th.small-offset-10{margin-left:83.33333%!important;Margin-left:83.33333%!important}table.body td.small-offset-11,table.body th.small-offset-11{margin-left:91.66667%!important;Margin-left:91.66667%!important}table.body table.columns td.expander,table.body table.columns th.expander{display:none!important}table.body .right-text-pad,table.body .text-pad-right{padding-left:10px!important}table.body .left-text-pad,table.body .text-pad-left{padding-right:10px!important}table.menu{width:100%!important}table.menu td,table.menu th{width:auto!important;display:inline-block!important}table.menu.small-vertical td,table.menu.small-vertical th,table.menu.vertical td,table.menu.vertical th{display:block!important}table.menu[align=center]{width:auto!important}table.button.expand{width:100%!important}}<\/style><table class=\"body\" data-made-with-foundation=\"\" style=\"Margin:0;background:#f3f3f3;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;width:100%\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><td class=\"center\" align=\"center\" valign=\"top\" style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\"><center data-parsed=\"\" style=\"min-width:580px;width:100%\"><table class=\"container text-center\" style=\"Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:580px\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><td style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\"><table class=\"row\" style=\"border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><th class=\"small-12 large-12 columns first last\" style=\"Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px\"><table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><th style=\"Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0;padding:0;text-align:left\"><center data-parsed=\"\" style=\"min-width:532px;width:100%\"><img src=\"http:\/\/192.241.214.123:3000\/img\/logo-250.png\" align=\"center\" class=\"text-center\" style=\"-ms-interpolation-mode:bicubic;Margin:0 auto;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:60px\"><\/center><h1 class=\"text-center\" style=\"Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:34px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center;word-wrap:normal\">Thank you for purchasing your Planeio Buyer\'s Guide<\/h1><p class=\"text-center\" style=\"Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0;margin-bottom:10px;padding:0;text-align:center\">You can view your report at any time using the link below.<\/p><table class=\"button large expand\" style=\"Margin:0 0 16px 0;border-collapse:collapse;border-spacing:0;margin:0 0 16px 0;padding:0;text-align:left;vertical-align:top;width:100%!important\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><td style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word\"><table style=\"border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%\"><tbody><tr style=\"padding:0;text-align:left;vertical-align:top\"><td style=\"-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background:#62B2B1;border:2px solid #62B2B1;border-collapse:collapse!important;color:#fefefe;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;width:auto!important;word-wrap:break-word\"><center data-parsed=\"\" style=\"min-width:0;width:100%\"><a href="' + secrets.baseUrl + '/myreports/' + charge.metadata.report_id + '" align=\"center\" class=\"text-center\" style=\"Margin:0;border:0 solid #62B2B1;border-radius:3px;color:#fefefe;display:inline-block;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:700;line-height:1.3;margin:0;padding:10px 20px 10px 20px;text-align:center;text-decoration:none\">View Report<\/a><\/center><\/td><\/tr><\/tbody><\/table><\/td><\/tr><\/tbody><\/table><hr style=\"Margin:20px auto;border-bottom:1px solid #cacaca;border-left:0;border-right:0;border-top:0;clear:both;height:0;margin:20px auto;max-width:580px\"><p style=\"Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0;margin-bottom:10px;padding:0;text-align:left\"><small style=\"color:#cacaca;font-size:80%\">You\'re getting this email because you\'ve signed up for Planeio.<\/small><\/p><\/th><th class=\"expander\" style=\"Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:19px;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0\"><\/th><\/tr><\/tbody><\/table><\/th><\/tr><\/tbody><\/table><\/td><\/tr><\/tbody><\/table><\/center><\/td><\/tr><\/tbody><\/table><\/body><\/html>',
                    text: emails.purchase_thanks.text
                };
                transporter.sendMail(mailOptions, function(err) {
                    if (err) {
                        console.log(err.message);
                    }
                });
            }
        );


    });
};

exports.getPromo = function(req, res) {
  //get gen report base on req.params.id
  GeneratedReport.find({_id: req.params.id, 'details.free_promo': true}, function(err, report){
    if(!report){res.status(404).send('Report Not Found');}
    if (err) {console.log(err)}
    if(report.length > 0 && report[0].details.free_promo == true && req.user.email_verified) {
      User.findByIdAndUpdate(
              req.user.id, {
                  $push: {
                      "purchased_reports": {
                          report_id: req.params.id,
                          date: Date.now()
                      }
                  }, 'free_report_claimed': true
              }, {
                  safe: true,
                  upsert: true
              },
              function(err, user) {
                  console.log(err);
                  req.flash('success', {
                          msg: 'Your free report has been added to your report list.'
                      });
                  res.redirect('/myreports');

              })
    } else {
      res.redirect('/myreports');
    }

  })
}
  // 
  // Set your secret key: remember to change this to your live secret key in production
  // See your keys here https://dashboard.stripe.com/account/apikeys

  // (Assuming you're using express - expressjs.com)
  // Get the credit card details submitted by the form
  // var stripeToken = req.body.stripeToken;

  // stripe.customers.create({
  //   source: stripeToken,
  //   description: 'Buyers Report'
  // }).then(function(customer) {
  //   return stripe.charges.create({
  //     amount: 3900, // amount in cents, again
  //     currency: "usd",
  //     customer: customer.id
  //   });
  // }).then(function(charge) {
  //   // YOUR CODE: Save the customer ID and other info in a database for later!
  //   console.log(charge);
  // });