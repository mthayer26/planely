var mongoose = require('mongoose');

var pageSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  url: String,
  body_html: String,
  make: String,
  parsed: { type: Boolean, default: false },
  scrape_id: mongoose.Schema.Types.ObjectId
});

module.exports = mongoose.model('Page', pageSchema);