//Dispatch all scraping jobs here

var CronJob = require('cron').CronJob;
var scraper = require('./scraper');
var reportGenerator = require('./report_generator');
var tests = require('./tests');

var mig = require('./price-migration');
//1) Kickoff cron jobs for scraping Beechcraft, Cessna, Piper, Mooney, Cirrus, Diamond

// exports.scrapeDispatch = function(req, res, next) {
    console.log("Initiating dispatcher...")
    

    new CronJob('00 00 4 * * 6', function() {
        scraper.pageCountScrape('BEECHCRAFT');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 30 4 * * 6', function() {
        scraper.pageCountScrape('CESSNA');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 00 5 * * 6', function() {
        scraper.pageCountScrape('MOONEY');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 30 5 * * 6', function() {
        scraper.pageCountScrape('PIPER');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 00 6 * * 6', function() {
        scraper.pageCountScrape('CIRRUS');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 30 6 * * 6', function() {
        scraper.pageCountScrape('DIAMOND');
    }, null, true, 'America/Los_Angeles');

    new CronJob('00 30 7 * * 6', function() {
        tests.runTests();
        reportGenerator.reportGenerator()
    }, null, true, 'America/Los_Angeles');
// }

