/**
 * GET /
 * all the datas
 */

var _ = require('lodash');
var numeral = require('numeral');
var moment = require('moment');

var PlaneModel = require('../models/Planemodel');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Report = require('../models/Report');

function filterOutliers(someArray) {  

    // Copy the values, rather than operating on references to existing values
    var values = someArray.concat();

    // Then sort
    values.sort( function(a, b) {
            return a - b;
         });

    /* Then find a generous IQR. This is generous because if (values.length / 4) 
     * is not an int, then really you should average the two elements on either 
     * side to find q1.
     */     
    var q1 = values[Math.floor((values.length / 4))];
    // Likewise for q3. 
    var q3 = values[Math.ceil((values.length * (3 / 4)))];
    var iqr = q3 - q1;

    // Then find min and max values
    var maxValue = q3 + iqr*1.5;
    var minValue = q1 - iqr*1.5;

    // Then filter anything beyond or beneath these values.
    var filteredValues = values.filter(function(x) {
        return (x <= maxValue) && (x >= minValue);
    });

    // Then return
    return filteredValues;
}

function getAve(someArray) {
    var total = 0;
    for(var i = 0; i < someArray.length; i++) {
        total += someArray[i];
    }
    return avg = total / someArray.length
}

exports.getFleet = function(req, res) {
    var q = req.query.q;
    var make = req.query.make;
    var payload = {};
    var priceData = [];
    var hourData = [];
    var dates = [];
    var counter = 0;
    // var q = '/.*'+q+'.*/';
    console.log(q);

    Plane.aggregate(
        { $match: { 'model_lower': new RegExp(q, "i"), 'make_lower': new RegExp(make, "i")}}, 
            {$group: {_id: "$scrape_id", count: { $sum: 1 }, AveragePrice: {$avg: "$price" }, AverageHours: {$avg: "$engine_tt" }, min: {$min: "$price"}, max: {$max: "$price"}}},
                { $sort: { _id: 1}}, function(err, data){

                    data.forEach(function(item){
                        
                        if(item.AveragePrice < 2000000 ){
                        timestamp = item._id.toString().substring(0,8);
                        date = new Date( parseInt( timestamp, 16 ) * 1000 );
                        dates.push(moment(date).format("MMM Do YY"));
                        priceData.push(Math.round(item.AveragePrice));
                        hourData.push(Math.round(item.AverageHours));
                        }
                    })

                    Plane.aggregate(
                        { $match: { 'model_lower': new RegExp(q, "i"), 'make_lower': new RegExp(make, "i")}}, {$group: {_id: "$model_lower", count: { $sum: 1 }, AveragePrice: {$avg: "$price" }, AverageHours: {$avg: "$engine_tt" }, min: {$min: "$price"}, max: {$max: "$price"} }},
                        { $sort: { AveragePrice: 1}}, function(err, data){

                        payload.priceData = priceData;
                        payload.makes = data;
                        // payload.priceData = filterOutliers(priceData);
                        payload.dates = dates;
                        payload.hourData = hourData;
                        payload.averageHour = numeral(Math.round(getAve(hourData))).format('0,0');
                        payload.averagePrice = numeral(Math.round(getAve(priceData))).format('0,0');
                        payload.averagePriceRaw = Math.round(getAve(priceData));
                        res.json(payload);

                    })
                    

                }
        )
}

exports.engineTimes = function(req, res) {
    var q = req.query.q;
    var make = req.query.make;
    engineTimeArr = [['plane', 'time']];
    Report.findOne({_id: req.params.id}, function(err, report){
        Plane.find({model_lower: new RegExp(q, "i"), make_lower: new RegExp(make, "i"), price: {$ne: null}, engine_tt: {$ne: null}}).select('engine_tt price model').exec(function(err, plane){
            // if (err) return next(err);
            // console.log(plane);
            // plane.forEach(function(item){
            //     var arr = [item.model, item.engine_tt]
            //     engineTimeArr.push(arr);
            // })
            // res.json(engineTimeArr);
            res.json(plane);
        })
    })
}

exports.priceEngineTimes = function(req, res) {
    var q = req.query.q;
    var make = req.query.make;
    engineTimeArr = [['price', 'time']];
    Report.findOne({_id: req.params.id}, function(err, report){
        Plane.find({model_lower: new RegExp(q, "i"), make_lower: new RegExp(make, "i"), price: {$ne: null}, engine_tt: {$ne: null}}).select('engine_tt price model').exec(function(err, plane){
            if (err) return next(err);
            console.log(plane);
            plane.forEach(function(item){
                var arr = [item.price, item.engine_tt]
                engineTimeArr.push(arr);
            })
            res.json(engineTimeArr);
        })
    })
}