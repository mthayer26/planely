/*-------------------------------------------------------------------------
   Parse the stored plane data
   - get the ave plane price for each make and model for that scrape
   - parse data for latest scrape then update scrape status
   - group by make, model, year

*-------------------------------------------------------------------------*/

var async = require('async');
var fs = require('fs');
var _ = require('underscore');

var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Parses = require('../models/Parses');
var PlaneModel = require('../models/Planemodel');
var PlaneMake = require('../models/Planemake');
var secrets = require('../config/secrets');

exports.parsePricesAveForModel = function(make, scrape_id) {
    // all_details = {};
    // make = "Beechcraft"
    // scrape_id = '55bc2623dde7f66068e5770f';
    // all_details.scrape_id = '55bc2623dde7f66068e5770f';
    // NEED TO CHANGE MAKE TO FIRST LETTER UPPERCASE ONLY
    makeLower = make.toLowerCase()
    make = makeLower.charAt(0).toUpperCase() + makeLower.slice(1);
    console.log('parse prices');
    console.log(make);
    console.log(scrape_id);
    parsePriceTimeByModel(make, scrape_id);
    parsePricesTimeByMake(make, scrape_id);
    parsePriceForYearByMake(make, scrape_id);
    updateScrapeStatus(scrape_id, 'Parsing Complete');
}
   

var parsePriceTimeByModel = function(make, scrape_id){
    Plane.aggregate(
        {$match: {"make" : make}},
        {$group: {_id:"$model", avg_price:{$avg:"$price"}, avg_hours:{$avg:"$engine_tt"}}}, {$sort: {_id: 1}},
        function (err, AvePrices) {
            console.log("pricetimebymodel");
            console.log(AvePrices);
            if (err) console.log("Error: " +err);
            for (i = 0; i < AvePrices.length; i++) {
                console.log(AvePrices[i]._id);
                PlaneModel.findOneAndUpdate({'model': AvePrices[i]._id}, {'make': make, 'model': AvePrices[i]._id}, {upsert: true}, function(){
                    if (err) return res.send(500, { error: err });
                    console.log('upserting complete');
                })
            };
            avePriceandHourModel(AvePrices);
            var parses = new Parses({
                raw_data: AvePrices,
                make: make
            })
            parses.save(function(err, scrape_id){
                if(err){return};
                console.log("parses saved")
            })
    } )  
}

var parsePricesTimeByMake = function(make, scrape_id){
//Average price and hours for make
    Plane.aggregate(
        {$match: {"make" : make}},
        {$group: {_id: make, avg_price:{$avg:"$price"}, avg_hours:{$avg:"$engine_tt"}}}, {$sort: {_id: 1}},
        function (err, AvePrice) {
            if (err) console.log("Error: " +err);
            console.log(AvePrice);
            data = avePriceAndHour(AvePrice)
            console.log(data);
            PlaneMake.update({make: make},{$push: {price_hour_trends: data }},function(err){
                if(err){
                        console.log(err);
                }else{
                        console.log("Successfully added cool stuff!");
                }
        });

            avePriceAndHour(AvePrice);
            var parses = new Parses({
                raw_data: AvePrice,
                make: make
            })
            parses.save(function(err, scrape_id){
                if(err){return};
                console.log("parses saved")
            })
    }
    );

}

var parsePriceForYearByMake = function(make, scrape_id){
//This is average price for make for each year
    Plane.aggregate(
        {$match: {"make" : make}},
        {$group: {_id:"$year", avg_price:{$avg:"$price"}, avg_hours:{$avg:"$engine_tt"}}}, {$sort: {_id: 1}},
        function (err, AvePriceYear) {
            if (err) console.log("Error: " +err);
            console.log(AvePriceYear);
            var parses = new Parses({
                raw_data: AvePriceYear,
                make: make
            })
            // avePriceYearModel(AvePriceYear);
            parses.save(function(err, scrape_id){
                if(err){return};
                console.log("parses saved")
                // res.redirect('/');
            })
    }
    );
}

var updateScrapeStatus = function(scrape_id, status){
    Scrape.findOneAndUpdate({'_id': scrape_id}, {'status': status}, {upsert:false}, function(err, doc){
        if (err){console.log(err)};
            console.log('status updated');
        });
}

//helper utilities
// [ { _id: 'Cirrus', avg_price: 290954.0588235294, avg_hours: 930.9775280898876 } ]
// to
// [price, hours]

var avePriceAndHour = function(data){
    console.log('avePriceAndHour');
    console.log(data);
    store = []
    date = new Date();
    store[0] = date;
    store[1] = data[0].avg_price;
    store[2] = data[0].avg_hours;
    // console.log(store);
    return store
}

var avePriceAndHourObj = function(data){
    store = []
    date = new Date();
    store[0] = date;
    store[1] = data.avg_price;
    store[2] = data.avg_hours;
    // console.log(store);
    return store
}

var avePriceAndYearObj = function(data){
    store = []
    date = new Date();
    store[0] = date;
    store[1] = data.avg_price;
    store[2] = data.avg_hours;
    // console.log(store);
    return store
}

indexPlanes = function(planeArray){

}

var avePriceandHourModel = function(arrayofmodels){
    for (i = 0; i < arrayofmodels.length; i++) {
        console.log(arrayofmodels[i]);
        PlaneModel.update({'model': arrayofmodels[i]._id},{$push: {price_hour_trends: avePriceAndHourObj(arrayofmodels[i]) }},function(err){
            if(err){
                    console.log(err);
            }else{
                    console.log("Successfully added price and hour!");
            }
        });
    }
}

var avePriceYearModel = function(arrayofmodels){
    for (i = 0; i < arrayofmodels.length; i++) {
        console.log(arrayofmodels[i]);
        PlaneModel.update({'model': arrayofmodels[i]._id},{$push: {price_per_year: avePriceAndHourObj(arrayofmodels[i]) }},function(err){
            if(err){
                    console.log(err);
            }else{
                    console.log("Successfully added year price!");
            }
        });
    }
}


