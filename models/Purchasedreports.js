var mongoose = require('mongoose');

var purchasedReportSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  user_id: { type: String, unique: true, lowercase: true },
  have_account: { type: Boolean, default: false },
});

module.exports = mongoose.model('NewsletterUsers', purchasedReportSchema);