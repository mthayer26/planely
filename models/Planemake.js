var mongoose = require('mongoose');

var planesmakeSchema = new mongoose.Schema({
  make: String,
  price_hour_trends: [], //yields a single number after each parse - each parse pushes another object
  hour_trends: [{ price: Number, date: Date }], //yields a single number after each parse - each parse pushes another object
  price_per_hour: [{ price: Number, hours: Number }], //yeild an object for each plane in parse comparing hours to price - this is overwritten each parse
});

module.exports = mongoose.model('Planemake', planesmakeSchema);