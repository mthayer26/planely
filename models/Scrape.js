var mongoose = require('mongoose');

var scrapeSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  make: String,
  make_upper: String,
  total_planes: Number,
  urls_to_parse: Array,
  status: { type: String, default: "Pending" }
});

module.exports = mongoose.model('Scrape', scrapeSchema);