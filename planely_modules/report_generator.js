var async = require('async');
var fs = require('fs');
var _ = require('underscore');
var numeral = require('numeral');
var moment = require('moment');

var Page = require('../models/Page');
var Report = require('../models/Report');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Parses = require('../models/Parses');
var PlaneModel = require('../models/Planemodel');
var PlaneMake = require('../models/Planemake');
var GeneratedReport = require('../models/GeneratedReport');
var secrets = require('../config/secrets');

exports.reportGenerator = function() {

    var today = moment().startOf('day')
    var tomorrow = moment(today).add(7, 'days')
    var full_report = {};

    Report.find({}, function(err, reports) {
        if(err){console.log(err)};
        reports.forEach(function(report) {
            var gr = new GeneratedReport();
            gr.details = report;
            gr.expires = moment().day(6).format("MM/DD/YYYY");
            //GET MODEL DATA -#of sale etc
            Plane.modelData(report, function(err, model_data){
                gr.model_data = model_data.model_data;
                //GET LOCATIONS
                Plane.map(report, function(err, map) {
                    if(err){console.log(err)};
                    gr.map = map
                    Plane.priceAndHour(report, function(err, priceAndHour) {
                        if(err){console.log(err)};
                        gr.priceAndHour = priceAndHour;
                        Plane.engineTimes(report, function(err, engineTimes) {
                            gr.engineTimes = engineTimes;
                            gr.save(function(err){
                                console.log(err);
                                if (err) return next(err);
                                console.log("saved");
                            });
                        })
                    })
                } )
            });
        })
    })
}