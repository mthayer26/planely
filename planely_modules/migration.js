var async = require('async');
var fs = require('fs');
var _ = require('underscore');

var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Parses = require('../models/Parses');
var PlaneModel = require('../models/Planemodel');
var PlaneMake = require('../models/Planemake');
var secrets = require('../config/secrets');

  console.log("Initiating migration...")


var counter = 0;

Plane.find({}).sort({"_id":-1}).skip(50000).limit(10000).exec(function(err, plane){
    plane.forEach(function(plane){
        Plane.findById(plane._id, function(err, plane){
            counter ++
            if(plane.make) {
                plane.make_lower = plane.make.toLowerCase();
                plane.manufacture = plane.make.toUpperCase();
            }else {
                console.log(plane._id);
            }

            if(plane.model) {
                plane.model_lower = plane.model.toLowerCase();
                plane.model = plane.model.toUpperCase();
            } else {
                console.log(plane._id);
            }
            plane.save(function(err){
                if (err) return handleError(err);
            })
                console.log(counter);
        })
    })
})
