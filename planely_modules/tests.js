var async = require('async');
var fs = require('fs');
var _ = require('underscore');
var moment = require('moment');
var async = require('async');

var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Parses = require('../models/Parses');
var PlaneModel = require('../models/Planemodel');
var PlaneMake = require('../models/Planemake');
var secrets = require('../config/secrets');

exports.runTests = function(){

    var nodemailer = require("nodemailer");
    var transporter = nodemailer.createTransport({
      service: 'SendGrid',
      auth: {
        user: secrets.sendgrid.user,
        pass: secrets.sendgrid.password
      }
    });

    console.log("Testing scrape...")

    var today = moment().startOf('day')
    var tomorrow = moment(today).add(1, 'days')
    console.log(today.toDate());
    console.log(tomorrow.toDate());
    counter = 0;

    Scrape.find({
      date: {
        $gte: today.toDate(),
        $lt: tomorrow.toDate()
      }
    }).exec(function(err, scrapes){
        
        plane_arr = []
        test_results = {
            scrape_count: scrapes.length,
            plane_count: plane_arr
        }

        async.each(scrapes, function(scrape, callback){
    
            Plane.find({scrape_id: scrape._id}).exec(function(err, planes) {
                plane_item = [planes[0].make, planes.length];
                plane_arr.push(plane_item);
                callback();
            })

        }, function(err){
            if(err){
                console.log('there was an error')
            }else {
                var from = "mthayer26@gmail.com";
                var body = JSON.stringify(test_results, null, 2);
                var to = 'mthayer26@gmail.com';
                var subject = 'Planely Scrape Results';

                var mailOptions = {
                    to: to,
                    from: from,
                    subject: subject,
                    text: body
                };

                transporter.sendMail(mailOptions, function(err) {
                    if (err) {
                        console.log(err)
                    }
                        console.log("Test executed and email sent")
                });
            }
        })
    })

}