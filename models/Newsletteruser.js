var mongoose = require('mongoose');

var newsletterUsersSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  email: { type: String, unique: true, lowercase: true },
  have_account: { type: Boolean, default: false },
});

module.exports = mongoose.model('NewsletterUsers', newsletterUsersSchema);