var mongoose = require('mongoose');

var purchasedReportSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  user_id: mongoose.Schema.Types.ObjectId,
  report_id: mongoose.Schema.Types.ObjectId,
  charge: mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('PurchasedReport', purchasedReportSchema);