var mongoose = require('mongoose');

var planemodelSchema = new mongoose.Schema({
    model: String,
    make: String,
    category: {type: Number, default: 0},
    description: {type: String, default: null},
    model_version: {type: String, default: null},
    image: {type: String, default: null},
    advisories: {type: String, default: null},
    ave_annual: {type: String, default: null},
    operating_cost_per_hour: {type: Number, default: 0},
    operating_cost_per_mile: {type: Number, default: 0},
    gph: {type: Number, default: 0},
    cruise: {type: Number, default: 0},
    useful_load: {type: Number, default: 0},
    add_to_report: { type: Boolean, default: false },
    price_hour_trends: Array, //yields a single number after each parse - each parse pushes another object
    hour_trends: [{ price: Number, date: Date }], //yields a single number after each parse - each parse pushes another object
    price_per_hour: [{ price: Number, hours: Number }], //yield an object for each plane in parse comparing hours to price - this is overwritten each parse 
    price_per_year: Array, //groups by years and get an average price 
});

module.exports = mongoose.model('Planemodel', planemodelSchema);