var _ = require('underscore');

var PlaneModel = require('../models/Planemodel');
var Scrape = require('../models/Scrape');
var Plane = require('../models/Plane')

/**
 * GET /
 * plane page.
 */
exports.index = function(req, res) {
  res.render('home', {
    title: 'Home'
  });
};

exports.getPlane = function(req, res) {
    var cityString ='http://maps.googleapis.com/maps/api/staticmap?center=Lebanon,+Kansas&zoom=3&scale=false&size=800x500&maptype=roadmap&format=png&visual_refresh=true';
    PlaneModel.findOne({_id: req.params.id}, function (err, plane) {
        Plane.find({model: plane.model}, function(err, allPlanes) {
             var locations = [];
             var counter = 0;
             allPlanes.forEach(function(item){
                var locationArray = item.location.split(',');
                if(locationArray[0] !== "USA") {
                    if(counter <= 20){
                        locations.push(locationArray[0]);
                        cityString += '&markers=size:mid%7Ccolor:0xff0000%7Clabel:1%7C'+locationArray[0];
                        counter ++;
                    }
                };
             })
            Scrape.find({make: plane.make}, function(err, scrapes){
                res.render('admin/plane', { 
                    title: plane.model,
                    plane: plane,
                    scrapes: scrapes,
                    imageSrc: cityString,
                    locations: _.uniq(locations)
                    });
            })
        })
    });
}

exports.getPlaneList = function(req, res) {
    console.log('get list');
    PlaneModel.find({}, function (err, planes) {
        res.render('admin/planeList', { 
            title: 'planes',
            planes: planes
            });
    });
}

exports.updatePlane = function(req, res) {
    PlaneModel.update({_id: req.params.id}, {$set: {
        model: req.body.model,
        make: req.body.make,
        category: req.body.category,
        description: req.body.description,
        image: req.body.image,
        ave_annual: req.body.ave_annual,
        operating_cost_per_hour: req.body.operating_cost_per_hour,
        operating_cost_per_mile: req.body.operating_cost_per_mile,
        gph: req.body.gph,
        cruise: req.body.cruise,
        useful_load: req.body.useful_load,
        add_to_report: req.body.add_to_report || false,
    }},
        function (err, team) {
            if (err) {
                console.log(err);
                req.flash('errors', {
                    msg: 'Something happened. Please try again.'
                });
                return res.redirect('/plane/'+req.params.id)
            }
            req.flash('success', {
                msg: 'Plane updated!'
            });
            return res.redirect('/admin/plane/'+req.params.id)
        });
    }