var mongoose = require('mongoose');

var generatedReportSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  details: mongoose.Schema.Types.Mixed,
  model_data: Array,
  map: mongoose.Schema.Types.Mixed,
  expires: String,
  priceAndHour: mongoose.Schema.Types.Mixed,
  engineTimes: mongoose.Schema.Types.Mixed
});

module.exports = mongoose.model('GeneratedReport', generatedReportSchema);