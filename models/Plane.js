var mongoose = require('mongoose');
var numeral = require('numeral');
var moment = require('moment');
var _ = require('lodash');

var planesSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  make: String,
  make_lower: String,
  make_readable: String,
  avionics: String,
  engines: String,
  year: Number,
  model: String,
  model_lower: String,
  manufacturer: String,
  interior: String,
  exterior: String,
  price: Number,
  reg_number: String,
  airframe_tt: Number,
  engine_tt: Number,
  prop_tt: Number,
  location: String,
  flight_rules: String,
  plane_all: String,
  description: String,
  scrape_id: mongoose.Schema.Types.ObjectId
});


// RETURNS 
// report.model_data
// count, ave hours, ave price, min price, max price
// for the report

planesSchema.statics.modelData = function(report, cb) {

    var today = moment().startOf('day')
    var tomorrow = moment(today).add(7, 'days')

    this.aggregate({
      $match: {
          'model_lower': new RegExp(report.model_query, "i"),
          'make_lower': new RegExp(report.make, "i"),
          // date: { $gte: today.toDate(), $lt: tomorrow.toDate() }
      }
  }, {
      $group: {
          _id: "$model",
          count: {
              $sum: 1
          },
          AveragePrice: {
              $avg: "$price"
          },
          AverageHours: {
              $avg: "$engine_tt"
          },
          min: {
              $min: "$price"
          },
          max: {
              $max: "$price"
          }
      }
  }, {
      $sort: {
          AveragePrice: 1
      }
  }, function(err, data) {
      data.forEach(function(item) {
              item.AverageHours = numeral(Math.round(item.AverageHours)).format('0,0');
              item.AveragePrice = numeral(Math.round(item.AveragePrice)).format('0,0');
              if (item.AveragePrice == 0 || item.AveragePrice == NaN) {
                  item.AveragePrice = "Not Available"
              };
          })
          //ADD MODEL DATA TO REPORT OBJ
      report.model_data = data;
      if (err) return cb(err);
      cb(null, report);

  })
}

// GET MAP
// 
planesSchema.statics.map = function(report, cb) {

    var today = moment().startOf('day');
    var tomorrow = moment(today).add(7, 'days');
    var map = {};
    var cityString = 'http://maps.googleapis.com/maps/api/staticmap?center=Lebanon,+Kansas&zoom=3&scale=2&size=800x400&maptype=roadmap&format=png&visual_refresh=true';

    this.find({
        'model_lower': new RegExp(report.model_query, "i"),
        'make_lower': new RegExp(report.make, "i"),
        // "date": {"$gte": today.toDate(), "$lt": tomorrow.toDate()}
    }, 

    function(err, allPlanes) {
        if (err) return cb(err);
        var locations = [];
        var counter = 0;
        allPlanes.forEach(function(item) {
            var locationArray = item.location.split(',');
            if (locationArray[0] !== "USA") {
                if (counter <= 20) {
                    locations.push(locationArray[0]);
                    cityString += '&markers=size:mid%7Ccolor:0x62B2B1%7C' + locationArray[0];
                    counter++;
                }
            };
        })
        map.locations = _.uniq(locations)
        map.image = cityString
        cb(null, map);
    })
}

// RETURNS 
// priceAndHour
// returns the series data including date, hours, price
// for the report

planesSchema.statics.priceAndHour = function(report, cb) {

  function getAve(someArray) {
    var total = 0;
    for(var i = 0; i < someArray.length; i++) {
        total += someArray[i];
    }
    return avg = total / someArray.length
}
    var payload = {};
    var priceData = [];
    var hourData = [];
    var dates = [];

    this.aggregate({
        $match: {
          'model_lower': new RegExp(report.model_query, "i"),
          'make_lower': new RegExp(report.make, "i"),
        }
    }, {
        $group: {
            _id: "$scrape_id",
            count: {
                $sum: 1
            },
            AveragePrice: {
                $avg: "$price"
            },
            AverageHours: {
                $avg: "$engine_tt"
            },
            min: {
                $min: "$price"
            },
            max: {
                $max: "$price"
            }
        }
    }, {
        $sort: {
            _id: 1
        }
    }, function(err, data) {
        if (err) return cb(err);
        data.forEach(function(item) {

            if (item.AveragePrice < 2000000) {
                timestamp = item._id.toString().substring(0, 8);
                date = new Date(parseInt(timestamp, 16) * 1000);
                dates.push(moment(date).format("MMM Do YY"));
                priceData.push(Math.round(item.AveragePrice));
                hourData.push(Math.round(item.AverageHours));
            }
        })
        payload.priceData = priceData;
        payload.dates = dates;
        payload.hourData = hourData;
        payload.averageHour = numeral(Math.round(getAve(hourData))).format('0,0');
        payload.averagePrice = numeral(Math.round(getAve(priceData))).format('0,0');
        payload.averagePriceRaw = Math.round(getAve(priceData));
        cb(null, payload);
    })
}

planesSchema.statics.engineTimes = function(report, cb) {

    engineTimeArr = [
        ['plane', 'time']
    ];

    this.find({
        model_lower: new RegExp(report.model_query, "i"),
        make_lower: new RegExp(report.make, "i"),
        price: {
            $ne: null
        },
        engine_tt: {
            $ne: null
        }
    }).select('engine_tt price model').exec(function(err, engineTimes) {
        if (err) return cb(err);
        cb(null, engineTimes);
    })
}

module.exports = mongoose.model('Plane', planesSchema);