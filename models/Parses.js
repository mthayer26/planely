var mongoose = require('mongoose');

var parsesSchema = new mongoose.Schema({
  date: { type: Date, default: Date.now },
  make: String,
  raw_data: Array
});

module.exports = mongoose.model('Parses', parsesSchema);