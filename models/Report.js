var mongoose = require('mongoose');

var reportSchema = new mongoose.Schema({
  // date: { type: Date, default: Date.now },
  title: String,
  make: String,
  model: String,
  general: String,
  performance: {
    cruise: Number,
    fuel_burn: String,
    useful_load: String,
    range: String
  },
  price_history_text: String,
  engine_time_distribution_text: String,
  ownership_cost: {
    ave_annual: String,
    ave_ownership_cost_hour: String,
    ave_cost_mile: String,
    insurance_cost: String
  },
  image: String,
  report_image: String,
  model_query: String,
  ads: [],
  useful_links: [],
  engine: String,
  report_price: Number,
  free_promo: Boolean,
  published: Boolean
});

module.exports = mongoose.model('Report', reportSchema);