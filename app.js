/**
 * Module dependencies.
 */
var express = require('express');
var fs = require('fs');
var cookieParser = require('cookie-parser');
var compress = require('compression');
var favicon = require('serve-favicon');
var session = require('express-session');
var bodyParser = require('body-parser');
var logger = require('morgan');
var errorHandler = require('errorhandler');
var lusca = require('lusca');
var methodOverride = require('method-override');

var _ = require('lodash');
var MongoStore = require('connect-mongo')(session);
var flash = require('express-flash');
var path = require('path');
var mongoose = require('mongoose');
var passport = require('passport');
var expressValidator = require('express-validator');
var connectAssets = require('connect-assets');

var scraper = require('./planely_modules/scraper');
var dispatcher = require('./planely_modules/dispatcher');
// var migration = require('./planely_modules/migration');
var parser = require('./planely_modules/parser');

/**
 * Controllers (route handlers).
 */
var homeController = require('./controllers/home');
var contactController = require('./controllers/contact');
var userController = require('./controllers/user');
var apiController = require('./controllers/api');
var planeController = require('./controllers/plane');
var reportController = require('./controllers/report');
var dataapiController = require('./controllers/dataapi');

/**
 * API keys and Passport configuration.
 */
var secrets = require('./config/secrets');
var passportConf = require('./config/passport');

/**
 * Create Express server.
 */
var app = express();
/**
 * Connect to MongoDB.
 */
mongoose.connect(secrets.db);
mongoose.connection.on('error', function() {
  console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
});

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(compress());
app.use(connectAssets({
  paths: [path.join(__dirname, 'public/css'), path.join(__dirname, 'public/js')]
}));
// app.use(logger('dev'));
// 
// // create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'})

// setup the logger
app.use(logger('combined', {stream: accessLogStream}))

app.use(favicon(path.join(__dirname, 'public/favicon.png')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(methodOverride());
app.use(cookieParser());
app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: secrets.sessionSecret,
  store: new MongoStore({ url: secrets.db, autoReconnect: true })
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(lusca({
  csrf: true,
  xframe: 'SAMEORIGIN',
  xssProtection: true
}));
app.use(function(req, res, next) {
  res.locals.user = req.user;
  next();
});
app.use(function(req, res, next) {
  if (/api/i.test(req.path)) req.session.returnTo = req.path;
  next();
});
app.use(express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));

/**
 * Primary app routes.
 */
app.get('/admin', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getReportList);

app.post('/', homeController.postStripe);
app.get('/', homeController.index);
app.get('/myreports', passportConf.isAuthenticated, homeController.myreports);
app.get('/myreports/:id', passportConf.isAuthenticated, homeController.myreportsById);
app.get('/myreports/:id/general', passportConf.isAuthenticated, homeController.myreportsGeneral);
app.get('/myreports/:id/price', passportConf.isAuthenticated, homeController.myreportsPrice);
app.get('/myreports/:id/costs', passportConf.isAuthenticated, homeController.myreportsCosts);
app.get('/myreports/:id/market', passportConf.isAuthenticated, homeController.myreportsMarket);
app.get('/myreports/:id/ads', passportConf.isAuthenticated, homeController.myreportsAds);
app.get('/myreports/:id/locations', passportConf.isAuthenticated, homeController.myreportsLocations);
app.get('/promo/:id', passportConf.isAuthenticated, homeController.getPromo);
app.get('/contact', contactController.getContact);
app.get('/account', userController.getAccount);
app.post('/contact', contactController.postContact);
app.get('/signup', userController.getSignup);
app.post('/signup', userController.postSignup);
app.get('/verify/:hash/:email', userController.getVerify);
app.get('/login', userController.getLogin);
app.post('/login', userController.postLogin);
app.get('/logout', userController.logout);
app.get('/forgot', userController.getForgot);
app.post('/forgot', userController.postForgot);
app.get('/reset/:token', userController.getReset);
app.post('/reset/:token', userController.postReset);
app.get('/admin/plane/:id', passportConf.needsGroup('admin'), planeController.getPlane);
app.post('/admin/plane/:id', passportConf.needsGroup('admin'), planeController.updatePlane);
app.get('/admin/plane', passportConf.needsGroup('admin'), passportConf.isAuthenticated, planeController.getPlaneList);

// app.get('/scrape', scraper.planeScrape);
// app.get('/dispatch', dispatcher.scrapeDispatch);
// app.get('/parse', parser.parsePricesAveForModel);


app.get('/admin/report', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getReportList);
app.get('/admin/users', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getUserList);
app.get('/admin/users/delete/:id', passportConf.isAuthenticated, passportConf.needsGroup('admin'), userController.deleteAccount);
app.get('/report/:id', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getReport);
app.get('/report/:id/general', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage2);
app.get('/report/:id/price', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage3);
app.get('/report/:id/costs', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage4);
app.get('/report/:id/market', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage5);
app.get('/report/:id/ads', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage6);
app.get('/report/:id/locations', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getPage7);
app.get('/admin/report/:id/edit', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.getEdit);
app.post('/admin/report/:id', passportConf.isAuthenticated, passportConf.needsGroup('admin'), reportController.postEdit);

// app.get('/scrape', scraper.planeScrape);
// app.get('/dispatch', dispatcher.scrapeDispatch);
app.get('/parse', parser.parsePricesAveForModel);

app.get('/dataapi/fleet', passportConf.isAuthenticated, dataapiController.getFleet);
app.get('/dataapi/enginetimes', passportConf.isAuthenticated, dataapiController.engineTimes);
app.get('/dataapi/priceenginetimes', passportConf.isAuthenticated, dataapiController.priceEngineTimes);

/**
 * API examples routes.
 */
app.get('/api', apiController.getApi);
app.get('/api/stripe', apiController.getStripe);
app.post('/api/stripe', apiController.postStripe);
app.get('/api/scraping', apiController.getScraping);
app.get('/api/facebook', passportConf.isAuthenticated, passportConf.isAuthorized, apiController.getFacebook);

/**
 * OAuth authentication routes. (Sign in)
 */

app.get('/auth/facebook', passport.authenticate('facebook', { scope: ['email', 'user_location'] }));
app.get('/auth/facebook/callback', passport.authenticate('facebook', { failureRedirect: '/login' }), function(req, res) {
  res.redirect('/');
  // res.redirect(req.session.returnTo || '/');
});

/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), function() {
  console.log('Express server listening on port %d in %s mode', app.get('port'), app.get('env'));
});

module.exports = app;
