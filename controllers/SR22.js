/**
 * GET /
 * report for Cessna 172.
 */

var PlaneModel = require('../models/Planemodel');
var Plane = require('../models/Plane');
var Report = require('../models/Report');

exports.getReportList = function(req, res) {
    Report.find({}, function(err, report){
        console.log('getting list');
        res.render('reportlist', { 
                        reports: report
                        });

    })
}


exports.getPage2 = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        res.render('reports/2', {
            report: report,
            reportUpper: report.title.toUpperCase()
        })
    })
}

exports.getSR22 = function(req, res) {

    //get all models that are SR variants using regex case insensitive
    // var name = 'SR';
    // PlaneModel.find({model: new RegExp(name, 'i')}, function(err, data) {
    //     console.log(data);
    //     // res.json(data);
    // })

    // //get the last 30 planes for sale
    // Plane.find({model: new RegExp(name, 'i')}).sort({"_id":1}).limit(30).exec(function(err, planeData) {
    //     console.log(planeData)
    //     // res.json(planeData);

    // })
    console.log(req.params.title);
    Report.findOne({_id: req.params.id}, function(err, report) {
        console.log(report);
        res.render('reports/1', {
            report: report,
            reportUpper: report.title.toUpperCase()
        })
    })



}

exports.getEdit = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        res.render('report', { 
                        title: report.title,
                        report: report,
                        reportUpper: report.title.toUpperCase()
                        });

    })
}

exports.postEdit = function(req, res) {
    // var report = new Report({
    //     title: "CIRRUS SR20/SR22",
    //     general: "An American piston-engine, four-or-five-seat, composite monoplane built by Cirrus Aircraft of Duluth, Minnesota. The Cirrus was the first production general aviation aircraft equipped with a parachute to lower the airplane safely to the ground after a loss of control, structural failure or mid-air collision. It was also the first manufactured aircraft with all-composite construction, flat-panel avionics and side-yoke flight controls",
    //     performance: {
    //     cruise: 155,
    //     fuel_burn: 12,
    //     useful_load: 912,
    //     range: 627
    //     },
    //     price_history_text: "The Fleet Price Trend graph shows the list price for Cessna 172’s for the last 2 years. ",
    //     engine_time_distribution_text: "This graph shows the distribution of planes by their engine time. The Ceesna 172 fleet has a low engine time distribution with the majority of the airplanes having less than 1000 hours of engine time.",
    //     ownership_cost: {
    //         ave_annual: "$2,000",
    //         ave_ownership_cost_hour: "$200",
    //         ave_cost_mile: "$10"
    //     }
    // });
    // 
    // console.log('UPDATING...');
    // console.log(req.body);
    // // res.send(req.body);
    Report.update({_id: req.body._id}, {$set: {
        title: req.body.title,
        general: req.body.general,
        price_history_text: req.body.price_history_text,
        engine_time_distribution_text: req.body.engine_time_distribution_text,
        image: req.body.image,
        'ownership_cost.ave_annual': req.body.ave_annual,
        'ownership_cost.ave_ownership_cost_hour': req.body.ave_ownership_cost_hour,
        'ownership_cost.ave_cost_mile': req.body.ave_cost_mile,
        'performance.fuel_burn': req.body.fuel_burn,
        'performance.cruise': req.body.cruise,
        'performance.useful_load': req.body.useful_load
    }},
        function (err, report) {
            if (err) {
                console.log(err);
                req.flash('errors', {
                    msg: 'Something happened. Please try again.'
                });
                return res.redirect('/reports/report');
            }
            req.flash('success', {
                msg: 'Report has been updated!'
            });
            return res.redirect('/report/'+req.body._id+'/edit')
        });

    // report.save(function(err){
    //     res.send('posted');
    // });
}