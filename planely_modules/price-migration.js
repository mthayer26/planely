var async = require('async');
var fs = require('fs');
var _ = require('underscore');
var cheerio = require('cheerio');

var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Parses = require('../models/Parses');
var PlaneModel = require('../models/Planemodel');
var PlaneMake = require('../models/Planemake');
var secrets = require('../config/secrets');

var mongoose = require('mongoose');

  console.log("Initiating price migration...")


// function objectIdWithTimestamp(timestamp) {
//     // Convert string date to Date object (otherwise assume timestamp is a date)
//     if (typeof(timestamp) == 'string') {
//         timestamp = new Date(timestamp);
//     }

//     // Convert date object to hex seconds since Unix epoch
//     var hexSeconds = Math.floor(timestamp/1000).toString(16);

//     // Create an ObjectId with that hex timestamp
//     var constructedObjectId = mongoose.Schema.Types.ObjectId(hexSeconds + "0000000000000000");

//     return constructedObjectId
// }


// Find all documents created after midnight on May 25th, 1980
// db.mycollection.find({ _id: { $gt: objectIdWithTimestamp('2016/06/10') } });

exports.mig = function() {

    var start_count = 0;
    var updated_count = 0;

    Page.find({'scrape_id' : {$gte : new mongoose.Types.ObjectId("575bc5213ce73b685ecf0251")}}).sort({"_id":-1}).skip(25000).limit(5000).exec(function(err, pages){
    // Page.find({}).sort({"_id": -1}).limit(200).exec(function(err, pages){
        console.log("looking for pages...");
        console.log(pages.length);
        start_count = pages.length;
        pages.forEach(function(page){
            console.log('Parcing price data');
            $ = cheerio.load(page.body_html);
            var price = $('.price').find('span[itemprop = "price"]').text();
                        price = price.trim();
                        if (price == 'Call for Price' || isNaN(price) || price == '') {price = null}
                            else {
                                price = parseInt(price.replace(/[^0-9\.]+/g, ""));
                            };

            var reg_number = $("label:contains('Registration #:')").parent().clone().children().remove().end().text().trim();
            var description = $('#detailed_desc').find('pre[itemprop = "description"]').text();

            make = $(".makeModel").find('span[itemprop = "name"]').text()
            make_lower = make.toLowerCase();
            if(price !== null){
                Plane.findOne({'scrape_id': page.scrape_id, 'reg_number': reg_number, 'description': description, 'make_lower': make_lower}).exec(function(err, plane){
                    if (plane !== null) {
                        var original_price = plane.price;
                        plane.price = price;
                        plane.save(function(err){
                            if (err) return handleError(err);
                            console.log(plane.reg_number+ " price was "+original_price+" updated with: "+ price);
                            updated_count ++;
                            console.log(updated_count);
                        })    
                    }

                })
            }
        })
    })
                
}
                