$(document).ready(function() {

    google.charts.load('current', {'packages':['corechart', 'scatter']});

    var options = {

    ///Boolean - Whether grid lines are shown across the chart
    scaleShowGridLines : true,

    //String - Colour of the grid lines
    scaleGridLineColor : "rgba(0,0,0,.05)",

    //Number - Width of the grid lines
    scaleGridLineWidth : 1,

    //Boolean - Whether to show horizontal lines (except X axis)
    scaleShowHorizontalLines: true,

    //Boolean - Whether to show vertical lines (except Y axis)
    scaleShowVerticalLines: true,

    //Boolean - Whether the line is curved between points
    bezierCurve : true,

    //Number - Tension of the bezier curve between points
    bezierCurveTension : 0.4,

    //Boolean - Whether to show a dot for each point
    pointDot : true,

    //Number - Radius of each point dot in pixels
    pointDotRadius : 4,

    //Number - Pixel width of point dot stroke
    pointDotStrokeWidth : 1,

    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
    pointHitDetectionRadius : 20,

    //Boolean - Whether to show a stroke for datasets
    datasetStroke : true,

    //Number - Pixel width of dataset stroke
    datasetStrokeWidth : 2,

    //Boolean - Whether to fill the dataset with a colour
    datasetFill : true,

    //String - A legend template
    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].strokeColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"

};

    var priceData = {
    labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
    datasets: [
        {
            label: "Price",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(90,178,177,1)",
            pointColor: "rgba(166,139,124,.5)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "rgba(166,139,124,1)",
            pointHighlightStroke: "rgba(220,220,220,1)"
        }
    ]
};

var hourData = {
    labels: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
    datasets: [
        {
            label: "Price",
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(90,178,177,1)",
            pointColor: "rgba(166,139,124,.5)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "rgba(166,139,124,1)",
            pointHighlightStroke: "rgba(220,220,220,1)"
        }
    ]
};

$.getJSON( "/dataapi/fleet?q="+local_data.model_query+"&make="+local_data.make, function( jsondata ) {
    var sum = 0;
    var dataToSum = jsondata.priceData.slice(-5);
    for( var i = 0; i < dataToSum.length; i++ ){
        sum += parseInt( dataToSum[i]); //don't forget to add the base
    }

    var avg = sum/dataToSum.length;

    if (avg <= Number(jsondata.averagePriceRaw)){
        $('#trend').removeClass('no-trend').addClass('trend-down');
    } else {
        $('#trend').removeClass('no-trend').addClass('trend-up');
    }


    $('.average-price').html("$"+jsondata.averagePrice);
    $('.average-hour').html(jsondata.averageHour);
    priceData.datasets[0].data = jsondata.priceData;
    hourData.datasets[0].data = jsondata.hourData;
    // data.datasets[1].data = jsondata.hourData;
    priceData.labels = jsondata.dates;
    hourData.labels = jsondata.dates;
    $('.loader').hide();
   
    var ctx = document.getElementById("fleet-price").getContext("2d");
    var fleetDetailsLineChart = new Chart(ctx).Line(priceData, options);

    var ctx = document.getElementById("fleet-hour").getContext("2d");
    var fleetDetailsLineChart = new Chart(ctx).Line(hourData, options);
    });

$.getJSON( "/dataapi/enginetimes?q="+local_data.model_query+"&make="+local_data.make, function(enginetimes) {
    engineTimeArr = [['Plane', 'Time']];
    priceTimeArr = [['Price', 'Time']];

    enginetimes.forEach(function(item){
                var arr1 = [item.model, item.engine_tt]
                engineTimeArr.push(arr1);
                var arr2 = [item.price, item.engine_tt]
                priceTimeArr.push(arr2);
            })
    $('.loader').hide();

//Drawing Chart 1

    google.charts.setOnLoadCallback(drawChart1);
    function drawChart1() {
    var Data1 = google.visualization.arrayToDataTable(engineTimeArr);

    var Options1 = {
        // title: 'Engine Time (Hours)',
        legend: { position: 'none' },
        colors: ['#62B2B1'],
        animation: {
            duration: 2,
            easing: 'out'
        },
        chartArea: {
            left: 0
        },
        chartArea: {
            width: 940
        }
    };

    var Chart1 = new google.visualization.Histogram(document.getElementById('chart_price'));

    Chart1.draw(Data1, Options1);
    }

//Drawing Chart 2

    google.charts.setOnLoadCallback(drawChart2);

      function drawChart2() {
        var Data2 = google.visualization.arrayToDataTable(priceTimeArr);

        var Options2 = {
            title: 'Price vs. Engine Time comparison',
            hAxis: {title: 'Price'},
            vAxis: {title: 'Engine Time'},
            legend: 'none',
            trendlines: {
              0: {
                type: 'exponential',
                visibleInLegend: true,
                color: '#A68B7C',
                lineWidth: 20,
                opacity: 0.2,
              }
            },
            colors: ['#62B2B1'],
            animation: {
                duration: 2,
                easing: 'out'
            },
            chartArea: {
                width: 940
            }
        };

        var Chart2 = new google.charts.Scatter(document.getElementById('chart_price_time'));
        Chart2.draw(Data2, google.charts.Scatter.convertOptions(Options2));
        // Chart2.draw(Data2, Options2);
      }



    })
});