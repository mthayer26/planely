var async = require('async');
var cheerio = require('cheerio');
var fs = require('fs');
var _ = require('underscore');
var mongodb = require('mongodb');
var request = require('request');
var request = request.defaults({
    jar: true
});
var Page = require('../models/Page');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var secrets = require('../config/secrets');

var parse = require('./parser');
var uri = 'mongodb://mthayer:planesPadres26@ds033069.mongolab.com:33069/planes';

planes = ['Cessna', 'Beechcraft', 'Piper', 'Cirrus', 'Mooney'];


//array of these
// {
//   "_id": ObjectId("55ba986132612648ac9231f5"),
//   "plane_all": "",
//   "make": "Cirrus",
//   "year": null,
//   "model": "SR20",
//   "manufacturer": "Cirrus",
//   "price": null,
//   "airframe_tt": null,
//   "engine_tt": null,
//   "prop_tt": null,
//   "location": "USA",
//   "flight_rules": "N/A",
//   "scrape_id": ObjectId("55ba985b32612648ac923193"),
//   "__v": 0
// }

