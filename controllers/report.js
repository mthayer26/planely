/**
 * GET /
 * report for Cessna 172.
 */

var _ = require('lodash');
var numeral = require('numeral');
var moment = require('moment');

var PlaneModel = require('../models/Planemodel');
var Plane = require('../models/Plane');
var Scrape = require('../models/Scrape');
var Report = require('../models/Report');
var User = require('../models/User');

exports.getReportList = function(req, res) {

    Report.find({}, function(err, report){
        console.log(report);
        res.render('admin/reportlist', { 
                        reports: report
                        })

    })
}

exports.getUserList = function(req, res) {
    User.find({}, function(err, users){
        res.render('admin/userlist', { 
                        users: users
                        })

    })
}


exports.getPage2 = function(req, res) {

    Report.findOne({_id: req.params.id}, function(err, report){
        if(report.length == 0){res.status(404).send('Not found');}
        res.render('reports/2', {
            title: "Page 2",
            report: report,
            reportUpper: report.title.toUpperCase(),
            expires: moment().day(6).format("MM/DD/YYYY")
        })
    })
}

exports.getPage3 = function(req, res) {

    Report.findOne({_id: req.params.id}, function(err, report){
        res.render('reports/3', {
            title: "Page 3",
            report: report,
            reportUpper: report.title.toUpperCase(),
            expires: moment().day(6).format("MM/DD/YYYY")
        })
    })
}

exports.getPage4 = function(req, res) {

    var today = moment().startOf('day')
    var tomorrow = moment(today).add(7, 'days')

    Report.findOne({_id: req.params.id}, function(err, report){
            Plane.aggregate(
                { $match: { 'model_lower': new RegExp(report.model_query, "i"), 'make_lower': new RegExp(report.make, "i"), date: {
        $gte: today.toDate(),
        $lt: tomorrow.toDate()
      }}}, {$group: {_id: "$model", count: { $sum: 1 }, AveragePrice: {$avg: "$price" }, AverageHours: {$avg: "$engine_tt" }, min: {$min: "$price"}, max: {$max: "$price"} }},
                { $sort: { AveragePrice: 1}}, function(err, data){
                    data.forEach(function(item){
                        item.AverageHours = numeral(Math.round(item.AverageHours)).format('0,0');
                        item.AveragePrice = numeral(Math.round(item.AveragePrice)).format('0,0');
                        if(item.AveragePrice == 0 || item.AveragePrice == NaN){item.AveragePrice = "Not Available"};
                    })
                res.render('reports/4', {
                    title: "Page 4",
                    report: report,
                    data: data,
                    reportUpper: report.title.toUpperCase(),
                    expires: moment().day(6).format("MM/DD/YYYY")
                })
            })
        // })
    })
}

exports.getPage5 = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        res.render('reports/5', {
            title: "Page 5",
            report: report,
            reportUpper: report.title.toUpperCase(),
            expires: moment().day(6).format("MM/DD/YYYY")
        })
    })
}

exports.getPage6 = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        res.render('reports/6', {
            title: "Page 6",
            report: report,
            reportUpper: report.title.toUpperCase(),
            expires: moment().day(6).format("MM/DD/YYYY")
        })
    })
}

exports.getPage7 = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        var cityString ='http://maps.googleapis.com/maps/api/staticmap?center=Lebanon,+Kansas&zoom=3&scale=2&size=800x400&maptype=roadmap&format=png&visual_refresh=true';
        Plane.find({ 'model_lower': new RegExp(report.model_query, "i"), 'make_lower': new RegExp(report.make, "i")}, function(err, allPlanes) {
             // console.log(allPlanes.length);
             var locations = [];
             var counter = 0;
             allPlanes.forEach(function(item){
                var locationArray = item.location.split(',');
                if(locationArray[0] !== "USA") {
                    if(counter <= 20){
                        locations.push(locationArray[0]);
                        cityString += '&markers=size:mid%7Ccolor:0x62B2B1%7C'+locationArray[0];
                        counter ++;
                    }
                };
             })
            res.render('reports/7', {
                title: "Page 7",
                report: report,
                reportUpper: report.title.toUpperCase(),
                expires: moment().day(6).format("MM/DD/YYYY"),
                imageSrc: cityString,
                locations: _.uniq(locations)
            })
        })
    });
}

exports.getReport = function(req, res) {

    //get all models that are SR variants using regex case insensitive
    // var name = 'SR';
    // PlaneModel.find({model: new RegExp(name, 'i')}, function(err, data) {
    //     console.log(data);
    //     // res.json(data);
    // })

    // //get the last 30 planes for sale
    // Plane.find({model: new RegExp(name, 'i')}).sort({"_id":1}).limit(30).exec(function(err, planeData) {
    //     console.log(planeData)
    //     // res.json(planeData);

    // })
    Report.findOne({_id: req.params.id}, function(err, report) {
        res.render('reports/1', {
            title: "Page 1",
            report: report,
            reportUpper: report.title.toUpperCase(),
            expires: moment().day(6).format("MM/DD/YYYY")
        })
    })



}

exports.getEdit = function(req, res) {
    Report.findOne({_id: req.params.id}, function(err, report){
        ads_arr = report.ads.join(", ");
        link_arr = report.useful_links.join(", ");
        res.render('admin/report', { 
                        title: report.title,
                        report: report,
                        reportUpper: report.title.toUpperCase(),
                        ads: ads_arr,
                        links: link_arr
                        });

    })
}

exports.postEdit = function(req, res) {
    // var report = new Report({
    //     title: "CIRRUS SR20/SR22",
    //     general: "An American piston-engine, four-or-five-seat, composite monoplane built by Cirrus Aircraft of Duluth, Minnesota. The Cirrus was the first production general aviation aircraft equipped with a parachute to lower the airplane safely to the ground after a loss of control, structural failure or mid-air collision. It was also the first manufactured aircraft with all-composite construction, flat-panel avionics and side-yoke flight controls",
    //     performance: {
    //     cruise: 155,
    //     fuel_burn: 12,
    //     useful_load: 912,
    //     range: 627
    //     },
    //     price_history_text: "The Fleet Price Trend graph shows the list price for Cessna 172’s for the last 2 years. ",
    //     engine_time_distribution_text: "This graph shows the distribution of planes by their engine time. The Ceesna 172 fleet has a low engine time distribution with the majority of the airplanes having less than 1000 hours of engine time.",
    //     ownership_cost: {
    //         ave_annual: "$2,000",
    //         ave_ownership_cost_hour: "$200",
    //         ave_cost_mile: "$10"
    //     }
    // });
    // 
    // res.send(req.body);
    Report.update({_id: req.body._id}, {$set: {
        title: req.body.title,
        general: req.body.general,
        price_history_text: req.body.price_history_text,
        engine_time_distribution_text: req.body.engine_time_distribution_text,
        image: req.body.image,
        report_image: req.body.report_image,
        'ownership_cost.ave_annual': req.body.ave_annual,
        'ownership_cost.ave_ownership_cost_hour': req.body.ave_ownership_cost_hour,
        'ownership_cost.ave_cost_mile': req.body.ave_cost_mile,
        'ownership_cost.insurance_cost': req.body.insurance_cost,
        'performance.range': req.body.range,
        'performance.fuel_burn': req.body.fuel_burn,
        'performance.cruise': req.body.cruise,
        'performance.useful_load': req.body.useful_load,
        model_query: req.body.model_query,
        ads: req.body.ads.split(','),
        useful_links: req.body.useful_links.split(','),
        engine: req.body.engine,
        published: req.body.published
    }},
        function (err, report) {
            if (err) {
                console.log(err);
                req.flash('errors', {
                    msg: 'Something happened. Please try again.'
                });
                return res.redirect('/admin/report/'+req.body._id+'/edit');
            }
            req.flash('success', {
                msg: 'Report has been updated!'
            });
            return res.redirect('/admin/report/'+req.body._id+'/edit')
        });

    // report.save(function(err){
    //     res.send('posted');
    // });
}